// Javascript - Introduction to Objects

// Object Structure
// Object = {
//     key: value, <-- Key value pair
//     key: value
// }

// * object can contain another object inside it
let person = {
  firstName: "John",
  lastName: "Smith",
  location: {
    city: "Tokyo",
    country: "Japan",
  },
  emails: ["john@mail.com", "johnsmith@mail.xyz"],
  brushTeeth: () => {
    console.log(this.firstName + " has brushed this teeth");
  },
};

console.log(person);
console.log(person.firstName);
console.log(person.lastName);
console.log(person.emails[0]);
console.log(person.length); // undefined

console.clear();
for (const key in person) {
  if (person.hasOwnProperty.call(person, key)) {
    // const element = person[key];
    console.log(person[key]);
  }
}

console.log(person["lastName"]);
console.log(person[1]);

// person.age = 7;
// console.log(person.age);

// console.clear();
// console.log(person);

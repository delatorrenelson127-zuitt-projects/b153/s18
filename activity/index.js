// JS Activity in Session #18
let trainer = {};

trainer.name = "Ash Ketchum";
trainer.age = 10;
trainer.pokemon = ["Pikachu", "Eevee", "Charizard"];
trainer.friends = ["Misty", "Brock", "Tracey"];
trainer.talk = function () {
  console.log("Pikachu! I choose you!");
};

console.log(trainer.name);
trainer.talk();
